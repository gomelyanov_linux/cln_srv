import socket

from const import HOST, PORT


def main():
    user_host = input(f'Enter server host: ')
    host = user_host if user_host else HOST
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sct:
        sct.connect((host, PORT))
        print(sct.recv(1024).decode(encoding='utf-8'))


if __name__ == '__main__':
    main()
