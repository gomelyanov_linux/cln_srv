import socket
from datetime import datetime

from const import HOST, PORT


def main():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sct:
        sct.bind((HOST, PORT))
        sct.listen(5)
        while True:
            conn, addr = sct.accept()
            with conn:
                while True:
                    data = datetime.now().strftime('%d.%m.%Y %H:%M').encode(encoding='utf-8')
                    if not data:
                        break
                    conn.sendall(data)
                    break


if __name__ == '__main__':
    main()
