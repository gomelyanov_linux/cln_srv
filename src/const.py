import os

from dotenv import load_dotenv, find_dotenv


load_dotenv(find_dotenv())

HOST = os.environ.get('HOST', '127.0.0.1')
PORT = os.environ.get('PORT', 1303)
